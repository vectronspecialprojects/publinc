/* eslint-disable no-console */
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock'
import mockRNCNetInfo from '@react-native-community/netinfo/jest/netinfo-mock.js'
import mockSafeAreaContext from 'react-native-safe-area-context/jest/mock'

jest.useFakeTimers()

jest.mock('react-native-safe-area-context', () => mockSafeAreaContext)

jest.mock('@react-navigation/native', () => ({
  NavigationContainer: children => jest.fn().mockReturnValue(() => children),
  createNavigationContainerRef: jest.fn(),
  useIsFocused: jest.fn()
}))

jest.mock('@react-navigation/stack', () => ({
  createNativeStackNavigator: jest.fn()
}))

jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage)

jest.mock('react-native-geolocation-service', () => ({
  addListener: jest.fn(),
  getCurrentPosition: jest.fn(),
  removeListeners: jest.fn(),
  requestAuthorization: jest.fn(),
  setConfiguration: jest.fn(),
  startObserving: jest.fn(),
  stopObserving: jest.fn()
}))

jest.mock('react-native-code-push', () => {
  const cp = () => app => app
  Object.assign(cp, {
    InstallMode: {},
    CheckFrequency: {},
    SyncStatus: {},
    UpdateState: {},
    DeploymentStatus: {},
    DEFAULT_UPDATE_DIALOG: {},

    allowRestart: jest.fn(),
    checkForUpdate: jest.fn(() => Promise.resolve(null)),
    disallowRestart: jest.fn(),
    getCurrentPackage: jest.fn(() => Promise.resolve(null)),
    getUpdateMetadata: jest.fn(() => Promise.resolve(null)),
    notifyAppReady: jest.fn(() => Promise.resolve()),
    restartApp: jest.fn(),
    sync: jest.fn(() => Promise.resolve(1)),
    clearUpdates: jest.fn()
  })
  return cp
})

jest.mock('@react-native-community/netinfo', () => mockRNCNetInfo)

jest.mock('@react-navigation/bottom-tabs', () => ({
  createBottomTabNavigator: children => jest.fn().mockReturnValue(() => children)
}))

jest.mock('react-native/Libraries/Utilities/Platform', () => {
  const platform = jest.requireActual('react-native/Libraries/Utilities/Platform')
  return {
    ...platform,
    constants: {
      ...platform.constants,
      reactNativeVersion: {
        major: 0,
        minor: 65,
        patch: 1
      }
    }
  }
})

jest.mock('react-redux', () => {
  const ActualReactRedux = jest.requireActual('react-redux')
  return ActualReactRedux
})

jest.mock('react-native-safe-area-context', () => {
  const inset = {top: 0, right: 0, bottom: 0, left: 0}
  return {
    SafeAreaProvider: jest.fn().mockImplementation(({children}) => children),
    SafeAreaConsumer: jest.fn().mockImplementation(({children}) => children(inset)),
    useSafeAreaInsets: jest.fn().mockImplementation(() => inset)
  }
})

// Ignore console.log / console.warn / console.error whilst running tests
console.log = () => {}
console.warn = () => {}
console.error = () => {}
console.groupCollapsed = () => {}
console.groupEnd = () => {}

// mock timers for animated components
jest.useFakeTimers()

global.__reanimatedWorkletInit = jest.fn()
