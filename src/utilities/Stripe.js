import {apiGetStripeKey} from './ApiManage'
import {
  createGooglePayPaymentMethod,
  initGooglePay,
  initStripe as initialStripe,
  createToken
} from '@stripe/stripe-react-native'
import {isGooglePaySupported} from '@stripe/stripe-react-native/src/functions'

export async function initStripe() {
  const res = await apiGetStripeKey()
  if (res.ok) {
    await initialStripe({
      publishableKey: res.data?.key
    })
  }
}

export async function createPaymentMethod() {
  const {token, error} = await createToken({type: 'Card'})
  if (error) {
    throw error
  }
  return token
}

export async function createPaymentGooglePay(publishableKey, data) {
  try {
    await initialStripe({
      publishableKey
    })
    await initGooglePay({
      testEnv: true,
      merchantName: '',
      countryCode: 'AU',
      billingAddressConfig: {
        format: 'FULL',
        isPhoneNumberRequired: true,
        isRequired: false
      },
      existingPaymentMethodRequired: false,
      isEmailRequired: true
    })
    const res = await isGooglePaySupported()
    if (!res) {
      throw new Error('unsupported')
    }
    const {error, paymentMethod} = await createGooglePayPaymentMethod({
      amount: +data.total_price,
      currencyCode: data.currencyCode || 'AUD'
    })
    if (error) {
      if (error.code === 'Canceled') {
        return {message: 'canceled'}
      }
      return {message: 'failed'}
    }
    return paymentMethod
  } catch (e) {
    if (e.message === 'unsupported') {
      return {message: 'unsupported'}
    }
    return e
  }
}
