import React from 'react'
import {View, Text} from 'react-native'
import HTML from 'react-native-render-html'
import {deviceWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import {defaultSystemFonts} from 'react-native-render-html'
import Colors from '../Themes/Colors'

const systemFonts = [...defaultSystemFonts, Fonts.openSans]
const Html = ({color = Colors().backgroundText, html, textAlign, onLinkPress}) => {
  return (
    <HTML
      source={{
        html: html
          ?.replace(/&lt;/g, '<')
          ?.replace(/&gt;/g, '>')
          ?.replace(/&nbsp;/g, ' ')
          ?.replace(/<br>/g, '')
      }}
      ignoredStyles={['backgroundColor', 'color']}
      tagsStyles={{
        div: {textAlign: textAlign, color: color, fontFamily: Fonts.openSans},
        li: {color: color, fontFamily: Fonts.openSans},
        p: {
          textAlign: textAlign,
          color: color,
          fontFamily: Fonts.openSans
        },
        ul: {marginTop: 20, color: color},
        del: {textDecorationLine: 'line-through', textDecorationStyle: 'solid'},
        ol: {color: color},
        blockquote: {color: color}
      }}
      baseStyle={{color: color, textAlign: textAlign}}
      systemFonts={systemFonts}
      renderers={{
        blockquote: ({TDefaultRenderer, ...customProps}) => {
          return (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{color: color}}>"</Text>
              <TDefaultRenderer {...customProps} style={{paddingHorizontal: 2, fontFamily: Fonts.openSans}} />
              <Text style={{color: color}}>"</Text>
            </View>
          )
        }
      }}
      renderersProps={{a: {onPress: onLinkPress}}}
      contentWidth={deviceWidth()}
    />
  )
}

export default Html
