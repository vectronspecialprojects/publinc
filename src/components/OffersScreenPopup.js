import React, {useCallback} from 'react'
import {View, StyleSheet, Image} from 'react-native'
import {responsiveHeight, responsiveWidth, deviceWidth} from '../Themes/Metrics'
import Modal from 'react-native-modal'
import Styles from '../Themes/Styles'
import {DialogText} from './Text'
import {DialogComponent} from './ComponentContainer'
import {NegativeButton, PositiveButton} from './ButtonView'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import Toast from './Toast'
import Alert from './Alert'
import {localize} from '../locale/I18nConfig'
import {useDispatch} from 'react-redux'

const OffersScreenPopup = ({onCancelPress, renderItem, isVisible}) => {
  const dispatch = useDispatch()

  const handleClaimOffer = useCallback(async () => {
    try {
      onCancelPress()
      dispatch(setGlobalIndicatorVisibility(true))
      const rest = await dispatch(infoServicesActions.claimOffer(renderItem))
      await dispatch(infoServicesActions.fetchOffers())
      Toast.success(rest)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch, renderItem, onCancelPress])

  if (renderItem) {
    return (
      <Modal isVisible={isVisible} animationIn="swing" animationOut="zoomOut">
        <DialogComponent style={styles.popupContainer}>
          <View style={{width: '100%', aspectRatio: 1}}>
            <Image style={styles.image} source={{uri: renderItem.image_square}} />
          </View>

          <View style={{padding: responsiveWidth(20)}}>
            <DialogText style={Styles.mediumCapBoldText}>{renderItem.heading}</DialogText>
            {renderItem?.products?.length !== 0 && (
              <DialogText
                style={{
                  ...Styles.mediumCapBoldText,
                  ...{
                    marginBottom: responsiveHeight(20),
                    marginTop: responsiveHeight(5)
                  }
                }}
              >
                {+renderItem?.products?.[0]?.product?.point_price
                  ? `${renderItem?.products?.[0]?.product?.point_price} Points`
                  : 'Free'}
              </DialogText>
            )}
            <DialogText style={Styles.xSmallCapText}>{renderItem?.desc_short}</DialogText>
          </View>

          {renderItem?.products?.length !== 0 ? (
            <View style={styles.popupButtonsBar}>
              <NegativeButton style={styles.button} onPress={onCancelPress} title={'Cancel'} />
              <PositiveButton style={styles.button} onPress={handleClaimOffer} title={'Redeem'} />
            </View>
          ) : (
            <View style={styles.popupButtonsBar}>
              <NegativeButton style={styles.buttonClose} onPress={onCancelPress} title={'Close'} />
            </View>
          )}
        </DialogComponent>
      </Modal>
    )
  }
  return null
}

const styles = StyleSheet.create({
  popupContainer: {
    alignSelf: 'center',
    width: deviceWidth() * 0.7,
    borderRadius: 15,
    overflow: 'hidden'
  },
  popupButtonsBar: {
    width: '100%',
    flexDirection: 'row',
    height: responsiveHeight(50),
    justifyContent: 'space-between',
    paddingBottom: responsiveHeight(5),
    paddingHorizontal: responsiveWidth(5)
  },
  button: {
    flex: 1,
    maxWidth: '49.5%'
  },
  buttonClose: {
    flex: 1
  },
  buttonTouch: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover'
  }
})

export default OffersScreenPopup
