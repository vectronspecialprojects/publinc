import React from 'react'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from './HeaderButton'
import Styles from '../Themes/Styles'
import {isIOS} from '../Themes/Metrics'
import {useSelector} from 'react-redux'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'

function HeaderLeftButton({navData, iconName, onPress, showHomeIcon = false}) {
  const {route, navigation} = navData || {}
  const tabMenus = useSelector(state => state.app.tabMenus)

  if (route?.params?.params?.showHomeIcon || showHomeIcon) {
    return (
      <HeaderButtons
        HeaderButtonComponent={props => <HeaderButton {...props} IconComponent={FontAwesome5Pro} />}
      >
        <Item
          style={Styles.drawer}
          title={'Home'}
          IconComponent={FontAwesome5Pro}
          iconName={tabMenus[0]?.icon}
          onPress={() => {
            navigation.goBack()
          }}
        />
      </HeaderButtons>
    )
  }
  return (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        style={Styles.drawer}
        title="Menu"
        iconName={iconName ? iconName : !isIOS() ? 'md-arrow-back' : 'ios-arrow-back'}
        onPress={() => {
          if (onPress) {
            onPress()
          } else {
            navigation.goBack()
          }
        }}
      />
    </HeaderButtons>
  )
}

export default HeaderLeftButton
