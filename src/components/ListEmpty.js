import React from 'react'
import {CardText} from './Text'
import {View, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight} from '../Themes/Metrics'
import Colors from '../Themes/Colors'

const ListEmpty = ({message}) => {
  return (
    <View>
      <CardText centered style={styles.text} color={Colors().backgroundText}>
        {message}
      </CardText>
    </View>
  )
}
export default ListEmpty

const styles = StyleSheet.create({
  text: {
    fontSize: responsiveFont(10),
    marginTop: responsiveHeight(10)
  }
})
