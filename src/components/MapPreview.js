import React from 'react'
import {Image, StyleSheet} from 'react-native'
import ENV from '../constants/env'
import {TouchableCmp} from './UtilityFunctions'

const MapPreview = props => {
  let imagePreviewUrl

  if (props.location) {
    imagePreviewUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${props.location.lat},${props.location.lng}&zoom=14&size=400x200&maptype=roadmap&markers=color:red%7Clabel:A%7C${props.location.lat},${props.location.lng}&key=${ENV.googleApiKey}`
  }

  return (
    <TouchableCmp onPress={props.onPress} style={{...styles.mapPreview, ...props.style}}>
      {props.location ? <Image style={styles.mapImage} source={{uri: imagePreviewUrl}} /> : props.children}
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  mapPreview: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  mapImage: {
    width: '100%',
    height: 200
  }
})

export default MapPreview
