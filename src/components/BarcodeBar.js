import React from 'react'
import {View, StyleSheet, ImageBackground} from 'react-native'
import Barcode from 'react-native-barcode-builder'
import Colors from '../Themes/Colors'
import {FontSizes, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Images from '../Themes/Images'
import {CardText} from './Text'

const BarcodeBar = ({value, style, width, title, textColor, text, titleStyle}) => {
  if (!value) {
    return (
      <View style={style}>
        <View style={styles.barcodeContainer}>
          <ImageBackground style={styles.barcode} source={Images.barcode}>
            <View style={styles.textWrapper}>
              <CardText centered color={textColor} fontSize={FontSizes.small}>
                Your membership barcode is being generated.
              </CardText>
            </View>
          </ImageBackground>
        </View>
      </View>
    )
  }
  return (
    <View style={style}>
      <View style={styles.barcodeContainer}>
        <Barcode
          value={value}
          format="CODE128B"
          width={width}
          lineColor={Colors().black}
          background={Colors().white}
        />
      </View>
      <CardText centered color={textColor} fontSize={FontSizes.small} style={titleStyle}>
        {title.concat(' ', text)}
      </CardText>
    </View>
  )
}

const styles = StyleSheet.create({
  barcodeContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(42),
    overflow: 'hidden'
  },
  barcode: {
    width: responsiveWidth(200),
    height: responsiveHeight(50),
    alignSelf: 'center'
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: responsiveWidth(2),
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default BarcodeBar
