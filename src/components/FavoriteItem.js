import React from 'react'
import {StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import {TouchableCmp} from './UtilityFunctions'
import FastImage from 'react-native-fast-image'
import {CardText} from './Text'
import ComponentContainer from './ComponentContainer'

function FavoriteItem({data, onPress}) {
  return (
    <TouchableCmp onPress={onPress}>
      <ComponentContainer style={styles.container}>
        <FastImage source={{uri: data?.image_square}} style={styles.image} />
        <CardText style={styles.name}>{data?.name}</CardText>
      </ComponentContainer>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: responsiveWidth(5),
    borderRadius: responsiveWidth(10),
    marginRight: responsiveWidth(5),
    marginBottom: responsiveHeight(12),
    width: responsiveWidth(104)
  },
  image: {
    width: responsiveWidth(94),
    height: responsiveWidth(94),
    borderRadius: responsiveWidth(10)
  },
  name: {
    fontSize: responsiveFont(10),
    fontFamily: Fonts.regular,
    textAlign: 'center',
    marginTop: responsiveHeight(10)
  }
})

export default FavoriteItem
