import React from 'react'
import {HeaderButton} from 'react-navigation-header-buttons'

import Colors from '../Themes/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'

const CustomHeaderButton = ({IconComponent, ...props}) => {
  return (
    <HeaderButton
      {...props}
      IconComponent={IconComponent || Ionicons}
      iconSize={23}
      color={Colors().heroText}
    />
  )
}

export default CustomHeaderButton
