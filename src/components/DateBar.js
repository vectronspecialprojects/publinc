import React from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import {HeroText} from './Text'

const DateBar = ({days, style, color, itemStyle}) => {
  return (
    <View style={[styles.bar, style]}>
      {days?.map(day => (
        <View key={day.order} style={[styles.dayFrame, itemStyle, {borderColor: color || Colors().heroText}]}>
          <HeroText style={!!color && {color: color}}>{day.name?.toUpperCase().substring(0, 3)}</HeroText>
        </View>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: responsiveHeight(10)
  },
  dayFrame: {
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: responsiveWidth(3),
    marginHorizontal: responsiveWidth(2)
  }
})

export default DateBar
