import React from 'react'
import {StyleSheet, Modal, TouchableOpacity} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'

import {PositiveButton} from './ButtonView'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ComponentContainer from './ComponentContainer'
import {CardText} from './Text'

export default function BottomSheet({
  visible,
  onClose,
  headerTitle,
  children,
  onConfirm,
  confirmButtonTitle,
  confirmButtonStyle,
  showConfirmButton = true,
  showCloseButton = true,
  titleStyle,
  containerStyle,
  titleContainerStyle,
  disabled,
  ButtonComponent = PositiveButton
}) {
  return (
    <Modal animationType={'slide'} visible={visible} transparent onRequestClose={onClose}>
      <TouchableOpacity
        onPress={() => {
          onClose?.()
        }}
        activeOpacity={1}
        style={styles.backdrop}
      >
        <ComponentContainer style={[styles.contentContainer, containerStyle]}>
          {headerTitle && (
            <ComponentContainer style={[styles.titleContainer, titleContainerStyle]}>
              {showCloseButton && (
                <TouchableOpacity
                  onPress={() => {
                    onClose?.()
                  }}
                >
                  <MaterialIcons name={'close'} size={30} color={Colors().gray} />
                </TouchableOpacity>
              )}
              <CardText style={[styles.title, titleStyle]}>{headerTitle}</CardText>
            </ComponentContainer>
          )}
          {children}
          {showConfirmButton && (
            <ButtonComponent
              disabled={disabled}
              onPress={onConfirm}
              title={confirmButtonTitle}
              style={[{marginTop: responsiveHeight(20)}, confirmButtonStyle]}
            />
          )}
        </ComponentContainer>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {
    paddingBottom: 44,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: responsiveHeight(13.5),
    paddingHorizontal: responsiveWidth(25)
  },
  title: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontFamily: Fonts.openSans,
    flex: 1
  },
  titleContainer: {
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginBottom: responsiveHeight(20),
    flexDirection: 'row'
  },
  backdrop: {flex: 1, backgroundColor: Colors().opacity, justifyContent: 'flex-end'}
})
