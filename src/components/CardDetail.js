import React from 'react'
import {View, StyleSheet, Share} from 'react-native'
import CardHeader from './CardHeader'
import Html from '../components/Html'
import CartBar from './CartBar'
import {onLinkPress} from './UtilityFunctions'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import DateBar from './DateBar'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'
import Toast from './Toast'
import {useSelector} from 'react-redux'
import {BackgroundText} from './Text'

const CardDetail = props => {
  const appFlags = useSelector(state => state.app.appFlags)

  function handleCartPress() {
    if (props.handleCartPress) {
      props.handleCartPress()
    } else {
      navigate(RouteKey.ProductListScreen, {
        products: props.products,
        listingId: props.data?.id,
        type: props.data?.type?.id
      })
    }
  }

  const handleShare = async () => {
    try {
      await Share.share(
        {
          title: props.title,
          message: `${props.title}\n${props.data?.desc_short}`,
          url:
            JSON.parse(props?.data?.venue?.social_links).filter(item => item?.platform === 'website')[0]
              ?.url || appFlags?.app_default_website
        },
        {
          dialogTitle: 'Pick an App'
        }
      )
    } catch (error) {
      Toast.info(error.message)
    }
  }

  return (
    <View>
      {!!props?.title && <CardHeader title={props.title} style={styles.cardStyle} />}
      {!!props?.venueName && (
        <BackgroundText style={[Styles.xSmallUpText, styles.venueName]}>{props.venueName}</BackgroundText>
      )}

      {!!props?.html && (
        <View style={styles.htmlContainer}>
          <Html html={props.html} onLinkPress={onLinkPress} textAlign={'center'} />
        </View>
      )}
      {!!props?.startTime && !!props?.endTime && (
        <BackgroundText style={styles.dateTime}>
          START: {props?.startTime} END: {props?.endTime}
        </BackgroundText>
      )}
      {!!props?.days && <DateBar days={props.days} color={Colors().backgroundText} />}
      <CartBar
        style={styles.cartBarContainer}
        favorite={props.favorite}
        onFavoritePress={props.onFavoritePress}
        onChatPress={props.onChatPress}
        onCartPress={handleCartPress}
        onSharePress={handleShare}
        allowBooking={props.allowBooking}
        allowChat={props.allowChat}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  venueName: {
    marginBottom: responsiveWidth(10),
    alignSelf: 'center'
  },
  dateTime: {
    alignSelf: 'center',
    marginTop: responsiveHeight(10)
  },
  cardStyle: {marginVertical: responsiveHeight(10)},
  cartBarContainer: {
    marginTop: responsiveHeight(30)
  },
  htmlContainer: {
    paddingHorizontal: responsiveWidth(20)
  }
})

export default CardDetail
