import React from 'react'
import {View} from 'react-native'
import Styles from '../Themes/Styles'
import {BackgroundText} from './Text'

const CardHeader = props => {
  return (
    <View style={{...Styles.centerContainer, ...props.style}}>
      <BackgroundText style={[Styles.header, props.titleStyle]}>{props.title}</BackgroundText>
    </View>
  )
}

export default CardHeader
