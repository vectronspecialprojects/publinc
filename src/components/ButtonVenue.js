import ButtonView from './ButtonView'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'
import Colors from '../Themes/Colors'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Metrics, {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import React from 'react'
import {StyleSheet} from 'react-native'
import {useSelector} from 'react-redux'
import {isTrue} from '../utilities/utils'
import ComponentContainer from './ComponentContainer'

const ButtonVenue = ({}) => {
  const profile = useSelector(state => state.infoServices.profile)
  const appFlags = useSelector(state => state.app.appFlags)
  if (!isTrue(appFlags?.isMultipleVenue)) {
    return null
  }
  return (
    <ComponentContainer backgroundColor={Colors().heroFill}>
      <ButtonView
        onPress={() => {
          navigate(RouteKey.PreferredVenueScreen)
        }}
        title={profile?.member?.current_preferred_venue_name || 'Venue name'}
        style={styles.venueButton}
        backgroundColor={Colors().venueSelectorFill}
        titleStyle={styles.titleStyle}
        textColor={Colors().venueSelectorText}
        rightIcon={<MaterialCommunityIcons name={'chevron-down'} size={22} color={Colors().cardText} />}
      />
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  venueButton: {
    height: responsiveHeight(38),
    marginHorizontal: responsiveWidth(20),
    borderRadius: responsiveHeight(19),
    marginBottom: responsiveHeight(10),
    paddingRight: Metrics.small
  },
  titleStyle: {
    textAlign: null
  }
})

export default ButtonVenue
