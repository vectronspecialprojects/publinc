import React from 'react'
import {View, StyleSheet} from 'react-native'
import {CardText} from './Text'
import TextInputView from './TextInputView'
import Metrics, {FontSizes} from '../Themes/Metrics'

function SearchBar({
  value,
  onChangeText,
  placeholder,
  autoFocus = false,
  onSubmitEditing,
  style,
  backgroundColor
}) {
  return (
    <View style={[styles.container, style]}>
      <CardText style={styles.title}>Search</CardText>
      <TextInputView
        value={value}
        autoFocus={autoFocus}
        onChangeText={onChangeText}
        underlineColorAndroid="transparent"
        placeholder={placeholder}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={'search'}
        inputStyle={styles.input}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginBottom: 20
  },
  input: {
    borderRadius: Metrics.borderRadius
  },
  title: {
    marginBottom: Metrics.tiny,
    fontSize: FontSizes.small
  }
})

export default SearchBar
