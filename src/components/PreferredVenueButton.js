import React from 'react'
import {View, StyleSheet} from 'react-native'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import {TouchableCmp} from './../components/UtilityFunctions'
import {responsiveHeight, responsiveFont, responsiveWidth} from '../Themes/Metrics'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {localize} from '../locale/I18nConfig'
import {CardText, HeroText} from './Text'
import ComponentContainer from './ComponentContainer'

const PreferredVenueButton = props => {
  return (
    <ComponentContainer style={[styles.preferredVenueBarContainer, props.style]}>
      <TouchableCmp activeOpacity={0.6} style={{flex: 1}} onPress={props.onPress}>
        <View style={styles.preferredVenueBar}>
          <View>
            <HeroText style={{...Styles.smallCapText, ...{textTransform: null}}}>
              {!props.venueName ? 'Venue name' : props.venueName}
            </HeroText>
            <CardText style={Styles.xSmallNormalText}>
              {!props.ifOnlineOrdering || props.ifOnlineOrdering === undefined
                ? localize('preferredVenueButton.onlineOrderingNotAvailable')
                : localize('preferredVenueButton.onlineOrderingAvailable')}
            </CardText>
          </View>
          <View style={{position: 'absolute', right: responsiveWidth(5)}}>
            <FontAwesome
              name={props.icon === 'down' ? 'chevron-circle-down' : 'chevron-circle-up'}
              size={responsiveFont(22)}
              color={Colors().cardText}
            />
          </View>
        </View>
      </TouchableCmp>
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  preferredVenueBarContainer: {
    width: '100%',
    height: responsiveHeight(55)
  },
  preferredVenueBar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  }
})

export default PreferredVenueButton
