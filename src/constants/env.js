import {Platform} from 'react-native'

const env = {
  production: 'production',
  staging: 'staging'
}

const appSecret = '2da489f7ac6cce34ddcda02ef126270b'
const appToken = '12a3e1a732ee'

export const buildEvn = env.production

const baseUrl = {
  staging: 'https://apiredcape.vecport.net/publinc/api/',
  production: 'https://apiredcape.vecport.net/publinc/api/'
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'BW_K-tG1-hinnfsZOYYg_2-uo_JbWnQo7kMyR',
    production: 'eou8Fiwk_FZoYA3pD-RBPxUsKOLa_8MWFb-js'
  },
  android: {
    staging: 'EjJarK0JzajDQLMb5B8cUvRjw0-krsh-xgzNq',
    production: 'jbxirZmAdDqfrBrHnUnDizZpUrWEJl4_Rd-8a'
  }
})

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: 'f97e6005-6327-4b91-8475-17a83128140c',
  buildEvn: buildEvn,
  codePushKey: codePushKey[buildEvn],
  baseUrl: baseUrl[buildEvn],
  appSecret,
  appToken
}

export default vars
