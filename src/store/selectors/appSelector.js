const getAppData = state => state.app

export const getAppFlags = state => getAppData(state).appFlags

export const getAppEmailNotAllowedDomains = state => getAppData(state).emailNotAllowedDomains
