import dayjs from 'dayjs'

const getInfoData = state => state.infoServices

export const getVouchers = state => getInfoData(state).vouchers
export const getActiveVouchers = state => {
  const vouchers = getVouchers(state)
  return vouchers?.filter(
    item => (!item.expire_date || dayjs(item.expire_date).isSameOrAfter(dayjs())) && !item.redeemed
  )
}
export const getUnActiveVouchers = state => {
  const vouchers = getVouchers(state)
  return vouchers.filter(item => dayjs(item.expire_date).isBefore(dayjs()) || item.redeemed)
}
export const getOffers = state => getInfoData(state).offers

export const getRegularEvents = state => getInfoData(state).regularEvents || []
export const getSpecialEvents = state => getInfoData(state).specialEvents || []
