import React, {useCallback, useMemo} from 'react'
import {TouchableOpacity, StyleSheet, View} from 'react-native'
import FastImage from 'react-native-fast-image'
import Metrics, {
  deviceWidth,
  responsiveFont,
  responsiveHeight,
  responsiveWidth
} from '../../../Themes/Metrics'
import {CardText, HeroText, Link} from '../../../components/Text'
import dayjs from 'dayjs'
import {useSelector} from 'react-redux'
import {getRegularEvents, getSpecialEvents} from '../../../store/selectors'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import {HoursFormatted, ListingType} from '../../../constants/constants'
import Fonts from '../../../Themes/Fonts'
import Row from '../../../components/Row'
import Carousel from 'react-native-reanimated-carousel'
import Colors from '../../../Themes/Colors'
import DateBar from '../../../components/DateBar'

const WhatOnHorizontal = ({data}) => {
  const regularEvents = useSelector(getRegularEvents)
  const specialEvents = useSelector(getSpecialEvents)

  const handleItemPress = useCallback((item, index) => {
    navigate(RouteKey.WhatsonDetailsScreen, {
      listing: item.listing
    })
  }, [])

  const renderItem = useCallback(
    ({item, index}) => {
      const {listing} = item

      const eventDays = JSON.parse(listing.payload).occurrence
      let time = ''

      if (listing?.type?.id === ListingType.special && listing?.time_start) {
        time =
          dayjs(listing?.time_start, HoursFormatted).format('H:mm') +
          ' till ' +
          dayjs(listing?.time_end, HoursFormatted).format('H:mm')
      } else if (listing?.datetime_start) {
        time =
          dayjs(listing?.datetime_start, HoursFormatted).format('H:mm') +
          ' till ' +
          dayjs(listing?.datetime_end, HoursFormatted).format('H:mm')
      } else {
        time = 'All day'
      }

      return (
        <TouchableOpacity style={styles.itemContainer} onPress={() => handleItemPress(item, index)}>
          <FastImage source={{uri: listing.image_square}} style={styles.image} resizeMode={'cover'} />
          <View style={styles.textContainer(listing?.image_card_show)}>
            {listing?.image_card_show && (
              <View>
                <HeroText style={styles.name}>{listing.heading}</HeroText>
                <HeroText>{time}</HeroText>
                <View style={{flexDirection: 'row'}}>
                  {eventDays?.length < 7 ? (
                    <DateBar days={eventDays} itemStyle={styles.itemDate} style={styles.dateContainer} />
                  ) : (
                    <HeroText>Every Day</HeroText>
                  )}
                </View>
              </View>
            )}
          </View>
        </TouchableOpacity>
      )
    },
    [handleItemPress]
  )

  const handleViewAll = useCallback(() => {
    navigate(RouteKey.WhatsonScreen, {
      params: data
    })
  }, [data])

  const listData = useMemo(() => {
    return [...regularEvents, ...specialEvents]
  }, [regularEvents, specialEvents])

  if (!listData?.length) {
    return null
  }

  return (
    <View style={styles.container}>
      <Row style={styles.row}>
        <CardText style={[styles.title, {color: data?.icon_color}]}>{data?.page_name}</CardText>
        <Link onPress={handleViewAll}>View All</Link>
      </Row>
      <Carousel
        loop={false}
        width={responsiveWidth(150 * 1.7) + Metrics.xxs}
        height={responsiveWidth(180)}
        style={styles.carouselContainer}
        data={listData}
        renderItem={renderItem}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: Metrics.marginTop * 2
  },
  image: {
    width: responsiveWidth(150 * 1.7),
    height: responsiveWidth(150),
    borderRadius: Metrics.voucherBorderRadius,
    marginBottom: Metrics.tiny
  },
  itemContainer: {
    marginTop: Metrics.marginTop,
    alignItems: 'flex-end'
  },
  expireText: {
    marginRight: Metrics.xxs
  },
  title: {
    flex: 1,
    fontFamily: Fonts.bold,
    fontSize: responsiveFont(16)
  },
  name: {
    fontFamily: Fonts.bold
  },
  row: {
    paddingHorizontal: Metrics.paddingHorizontal
  },
  carouselContainer: {
    width: deviceWidth()
  },
  textContainer: opacity => ({
    position: 'absolute',
    width: responsiveWidth(150 * 1.7),
    height: responsiveWidth(150),
    backgroundColor: opacity ? Colors().opacity : 'transparent',
    borderRadius: Metrics.voucherBorderRadius,
    justifyContent: 'flex-end',
    paddingBottom: Metrics.marginTop,
    paddingHorizontal: Metrics.paddingHorizontal
  }),
  dayContainer: borderColor => ({
    borderWidth: 1,
    borderColor,
    height: responsiveHeight(18),
    borderRadius: responsiveHeight(9),
    paddingHorizontal: responsiveWidth(5)
  }),
  itemDate: {
    borderRadius: responsiveHeight(20)
  },
  dateContainer: {
    marginTop: Metrics.tiny
  }
})

export default WhatOnHorizontal
