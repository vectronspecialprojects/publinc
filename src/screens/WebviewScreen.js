import React, {useMemo, useEffect} from 'react'
import {View, ActivityIndicator, StyleSheet} from 'react-native'
import {useSelector} from 'react-redux'
import {WebView} from 'react-native-webview'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ScreenContainer from '../components/ScreenContainer'

const WebviewScreen = props => {
  const curentPreferredParams = useSelector(
    state => state.infoServices.profile?.member?.current_preferred_venue_full
  )

  const uri = useMemo(() => {
    if (props?.route?.params?.params?.page_layout === 'special_view') {
      if (props?.route?.params?.params?.special_page?.substr(0, 4) === 'link') {
        if (props?.route?.params?.params?.special_page === 'link1') {
          return curentPreferredParams?.link1
        }
        if (props?.route?.params?.params?.special_page === 'link2') {
          return curentPreferredParams?.link2
        }
        if (props?.route?.params?.params?.special_page === 'link3') {
          return curentPreferredParams?.link3
        }
      }
      if (props?.route?.params?.params?.special_page === 'website') {
        return props?.route?.params?.params?.special_link
      }
    }
    if (props?.route?.params?.params?.appUri) {
      return props?.route?.params?.params?.appUri
    }
  }, [curentPreferredParams, props])

  useEffect(() => {
    if (!uri) {
      Alert.alert(localize('webView.titleAlert'), localize('webView.message'), [{text: localize('okay')}])
    }
  }, [uri])

  return (
    <ScreenContainer>
      <WebView
        startInLoadingState={true}
        source={{uri: uri}}
        enableApplePay={true}
        automaticallyAdjustContentInsets={true}
        useWebKit={true}
        renderLoading={() => (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" />
          </View>
        )}
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: '50%'
  }
})

export default WebviewScreen
