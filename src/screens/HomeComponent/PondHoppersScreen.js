import React, {useMemo} from 'react'
import {StyleSheet, ActivityIndicator} from 'react-native'
import {useSelector} from 'react-redux'
import {WebView} from 'react-native-webview'
import ScreenContainer from '../../components/ScreenContainer'
import ComponentContainer from '../../components/ComponentContainer'

const ACTIONS = ['impact', 'ourstory', 'faqs']

function PondHoppersScreen({route}) {
  const {action, params} = route?.params

  const profile = useSelector(state => state.infoServices.profile)
  const pondHopper = useSelector(state => state.app.pondHopper)

  const pondHopperAction = useMemo(() => {
    if (action) {
      return action
    }
    if (params?.special_page === 'community-points') {
      return 'PLEDGE'
    }
    if (params?.special_page) {
      return params?.special_page?.toUpperCase()
    }
    return 'PLEDGE'
  }, [action, params])

  const body = useMemo(() => {
    let obj = {
      uniqueKey: pondHopper?.uniqueKey,
      action: pondHopperAction
    }
    if (!ACTIONS.includes(pondHopperAction.toLowerCase())) {
      obj = {
        ...obj,
        memberNumber: profile?.member?.bepoz_account_number || 'pending',
        venueId: `${profile?.member?.current_preferred_venue_full?.bepoz_venue_id || ''}`,
        firstName: profile?.member?.first_name,
        lastName: profile?.member?.last_name,
        email: profile?.email,
        mobile: profile?.mobile
      }
    }
    return obj
  }, [pondHopperAction, pondHopper, profile])

  return (
    <ScreenContainer>
      <WebView
        startInLoadingState={true}
        source={{
          uri: pondHopper?.url,
          method: 'POST',
          headers: {'Content-Type': 'text/plain'},
          body: JSON.stringify(body)
        }}
        renderLoading={() => (
          <ComponentContainer style={styles.loadingContainer}>
            <ActivityIndicator size="large" />
          </ComponentContainer>
        )}
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loadingContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: '50%'
  }
})

export default PondHoppersScreen
