import React, {useCallback} from 'react'
import {TouchableOpacity, StyleSheet, View} from 'react-native'
import FastImage from 'react-native-fast-image'
import Metrics, {
  deviceWidth,
  responsiveFont,
  responsiveHeight,
  responsiveWidth
} from '../../../Themes/Metrics'
import {BackgroundText, CardText, HeroText, Link} from '../../../components/Text'
import dayjs from 'dayjs'
import {useSelector} from 'react-redux'
import {getActiveVouchers} from '../../../store/selectors'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import {DateTimeFormatted} from '../../../constants/constants'
import Fonts from '../../../Themes/Fonts'
import Row from '../../../components/Row'
import Carousel from 'react-native-reanimated-carousel'
import Colors from '../../../Themes/Colors'

const VoucherComponent = ({data}) => {
  const vouchers = useSelector(getActiveVouchers)

  const handleItemPress = useCallback((item, index) => {
    navigate(RouteKey.VoucherDetailScreen, {
      voucherDetail: item,
      voucherIndex: index
    })
  }, [])

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity style={styles.itemContainer} onPress={() => handleItemPress(item, index)}>
          <FastImage source={{uri: item.image_card}} style={styles.image} resizeMode={'cover'} />
          <View style={styles.textContainer(item?.image_card_show)}>
            {item?.image_card_show && (
              <HeroText fontSize={responsiveFont(16)}>{item?.claim_promotion?.product?.name}</HeroText>
            )}
          </View>
          {!!item.expire_date && (
            <BackgroundText fontSize={responsiveFont(8)} style={styles.expireText}>
              Expire: {dayjs(item.expire_date).format(DateTimeFormatted)}
            </BackgroundText>
          )}
        </TouchableOpacity>
      )
    },
    [handleItemPress]
  )

  const handleViewAll = useCallback(() => {
    navigate(RouteKey.VouchersScreen, {
      params: data
    })
  }, [data])

  if (!vouchers?.length) {
    return null
  }

  return (
    <View style={styles.container}>
      <Row style={styles.row}>
        <CardText style={[styles.title, {color: data?.icon_color}]}>{data?.page_name}</CardText>
        <Link onPress={handleViewAll}>View All</Link>
      </Row>
      <Carousel
        loop={false}
        width={responsiveWidth(150 * 1.7) + Metrics.xxs}
        height={responsiveWidth(180)}
        style={styles.carouselContainer}
        data={vouchers}
        renderItem={renderItem}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: Metrics.marginTop * 2
  },
  image: {
    width: responsiveWidth(150 * 1.7),
    height: responsiveWidth(150),
    borderRadius: Metrics.voucherBorderRadius,
    marginBottom: Metrics.tiny
  },
  itemContainer: {
    marginTop: Metrics.marginTop,
    alignItems: 'flex-end'
  },
  expireText: {
    marginRight: Metrics.xxs
  },
  title: {
    fontSize: responsiveFont(18),
    flex: 1,
    fontFamily: Fonts.openSansBold,
    marginBottom: responsiveHeight(8)
  },
  row: {
    paddingHorizontal: Metrics.paddingHorizontal
  },
  carouselContainer: {
    width: deviceWidth()
  },
  textContainer: opacity => ({
    position: 'absolute',
    width: responsiveWidth(150 * 1.7),
    height: responsiveWidth(150),
    backgroundColor: opacity ? Colors().opacity : 'transparent',
    borderRadius: Metrics.voucherBorderRadius,
    justifyContent: 'flex-end',
    paddingBottom: Metrics.marginTop,
    paddingHorizontal: Metrics.paddingHorizontal
  })
})

export default VoucherComponent
