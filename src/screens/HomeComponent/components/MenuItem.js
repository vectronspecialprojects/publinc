import React, {useCallback} from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Fonts from '../../../Themes/Fonts'
import CustomIcon from '../../../components/CustomIcon'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import CardContainer from '../../../components/CardContainer'
import {CardText, HeroText, Link} from '../../../components/Text'
import {useSelector} from 'react-redux'
import {isTrue} from '../../../utilities/utils'

function MenuItem({
  title,
  subTitle,
  dataTitle,
  style,
  iconImage,
  iconName,
  value,
  onPress,
  valueRight = true,
  color,
  iconSelector,
  disabled,
  actionSubTitle,
  children,
  cardStyle
}) {
  const appFlags = useSelector(state => state.app.appFlags)

  const handleSubTitlePress = useCallback(() => {
    if (actionSubTitle) {
      navigate(RouteKey.PondHoppersScreen, {
        action: actionSubTitle
      })
    } else {
      onPress?.()
    }
  }, [onPress, actionSubTitle])

  return (
    <View style={[styles.container, style]}>
      {!!title && (
        <View style={styles.row}>
          <CardText style={styles.title}>{title}</CardText>
          {!!subTitle && (
            <Link style={styles.subTitle} onPress={handleSubTitlePress}>
              {subTitle}
            </Link>
          )}
        </View>
      )}
      <CardContainer
        disabled={disabled}
        onPress={onPress}
        isShadow={isTrue(appFlags.app_is_component_shadowed)}
        style={[styles.buttonContainer, cardStyle]}
      >
        <View style={styles.contentContainer}>
          <CustomIcon
            image_icon={iconImage}
            icon_selector={iconSelector}
            name={iconName}
            size={responsiveFont(35)}
            color={color || Colors().cardText}
            tintColor={color || Colors().cardText}
          />
          <View style={{flex: 1, justifyContent: 'center', marginLeft: responsiveWidth(15)}}>
            {!!dataTitle && (
              <CardText style={styles.itemTitle} color={color}>
                {dataTitle}
              </CardText>
            )}
            {!valueRight && !!value && <CardText style={styles.itemValue}>{value}</CardText>}
          </View>
          {valueRight && !!value && (
            <View style={[styles.countWrapper(Colors().heroFill)]}>
              <HeroText style={styles.itemValue}>{value}</HeroText>
            </View>
          )}
        </View>
        {children}
      </CardContainer>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: responsiveHeight(15),
    marginHorizontal: responsiveWidth(15)
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  title: {
    fontSize: responsiveFont(18),
    flex: 1,
    fontFamily: Fonts.openSansBold,
    marginBottom: responsiveHeight(8)
  },
  subTitle: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(12)
  },
  buttonContainer: {
    borderRadius: responsiveWidth(15),
    paddingVertical: responsiveWidth(12),
    paddingHorizontal: responsiveWidth(12)
  },
  contentContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  itemTitle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16)
  },
  itemValue: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(12),
    marginRight: responsiveWidth(2),
    marginLeft: responsiveWidth(2)
  },
  countWrapper: backgroundColor => ({
    minWidth: responsiveHeight(20),
    maxHeight: responsiveHeight(30),
    borderRadius: 50,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor
  })
})
export default MenuItem
