import React, {useCallback, useState} from 'react'
import {FlatList, TouchableOpacity, StyleSheet, View} from 'react-native'
import FastImage from 'react-native-fast-image'
import Metrics, {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import {BackgroundText, CardText, Link} from '../../../components/Text'
import dayjs from 'dayjs'
import {useSelector} from 'react-redux'
import {getOffers} from '../../../store/selectors'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import Fonts from '../../../Themes/Fonts'
import Row from '../../../components/Row'
import OffersScreenPopup from '../../../components/OffersScreenPopup'
import {isTrue} from '../../../utilities/utils'

const OfferComponent = ({data}) => {
  const offers = useSelector(getOffers)
  const [selectedItem, setSelectedItem] = useState(null)

  const handleItemPress = useCallback((item, index) => {
    setSelectedItem(item)
  }, [])

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity style={styles.itemContainer} onPress={() => handleItemPress(item, index)}>
          <FastImage source={{uri: item?.listing?.image_square}} style={styles.image} resizeMode={'cover'} />
          <View style={styles.contentContainer}>
            <BackgroundText style={styles.name} numberOfLines={2}>
              {item?.listing?.name}
            </BackgroundText>
            {!isTrue(item.never_expired) && (
              <BackgroundText fontSize={responsiveFont(8)} style={styles.expireText}>
                Expire: {dayjs(item.date_end).format('DD/MM/YYYY')}
              </BackgroundText>
            )}
          </View>
        </TouchableOpacity>
      )
    },
    [handleItemPress]
  )

  const handleViewAll = useCallback(() => {
    navigate(RouteKey.OffersScreen, {
      params: data
    })
  }, [data])

  const handleCancel = useCallback(() => {
    setSelectedItem('')
  }, [])

  if (!offers?.length) {
    return null
  }

  return (
    <View style={styles.container}>
      <Row style={styles.row}>
        <CardText style={[styles.title, {color: data?.icon_color}]}>{data?.page_name}</CardText>
        <Link onPress={handleViewAll}>View All</Link>
      </Row>
      <FlatList
        horizontal
        data={offers}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
      />
      <OffersScreenPopup
        isVisible={!!selectedItem}
        renderItem={selectedItem?.listing}
        onCancelPress={handleCancel}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: Metrics.marginTop * 2
  },
  image: {
    width: responsiveWidth(105),
    height: responsiveWidth(105),
    borderRadius: Metrics.voucherBorderRadius,
    marginBottom: Metrics.tiny,
    ...shadow
  },
  itemContainer: {
    marginLeft: Metrics.xxs,
    marginTop: Metrics.marginTop,
    width: responsiveWidth(105)
  },
  expireText: {
    marginRight: Metrics.xxs
  },
  name: {
    fontSize: responsiveFont(10),
    fontFamily: Fonts.bold,
    flex: 1
  },
  contentContainer: {
    paddingLeft: Metrics.xxs
  },
  title: {
    fontSize: responsiveFont(18),
    flex: 1,
    fontFamily: Fonts.openSansBold,
    marginBottom: responsiveHeight(8)
  },
  row: {
    paddingHorizontal: Metrics.paddingHorizontal
  }
})

export default OfferComponent
