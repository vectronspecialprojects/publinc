import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Metrics, {deviceWidth, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import {isTrue} from '../../../utilities/utils'
import MenuItem from './MenuItem'
import {getNumberData} from './contanst'
import CommunityDataComponent from './CommunityDataComponent'
import {useSelector} from 'react-redux'
import LabelComponent from './LabelComponent'
import ButtonVenue from '../../../components/ButtonVenue'
import GalleryComponent from './GalleryComponent'
import CustomRefreshControl from '../../../components/CustomRefreshControl'
import Notification from '../../../components/Notification'
import BarcodeBar from '../../../components/BarcodeBar'
import Colors from '../../../Themes/Colors'
import {localize} from '../../../locale/I18nConfig'
import OfferComponent from './OfferComponent'
import VoucherComponent from './VoucherComponent'
import WhatOnHorizontal from '../../WhatOnComponent/components/WhatOnHorizonal'

function HomeVersion2({
  isRefreshing,
  setIsRefreshing,
  loadContent,
  communityImpact,
  pledge,
  profile,
  internetState,
  homeMenus,
  ifRoute,
  notifications
}) {
  const pondHoppersEnable = useSelector(state => state.app.pondHoppersEnable)
  return (
    <View style={styles.container}>
      <ButtonVenue />
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <CustomRefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                if (internetState) {
                  setIsRefreshing(true)
                  loadContent()
                }
              }}
            />
          }
        >
          {notifications?.length >= 1 && <Notification notifications={notifications} />}
          {homeMenus?.map((item, index) => {
            switch (item.special_page) {
              case 'community-points':
                return (
                  <CommunityDataComponent
                    key={index.toString()}
                    communityImpact={communityImpact}
                    pledge={pledge}
                    item={item}
                    ifRoute={ifRoute}
                    profile={profile}
                  />
                )
              case 'points':
                return (
                  <MenuItem
                    key={index.toString()}
                    title={'Your membership'}
                    subTitle={isTrue(pondHoppersEnable) && 'Explore perks'}
                    actionSubTitle={'PERKS'} //Just hard code
                    style={{marginTop: responsiveHeight(15)}}
                    color={item.icon_color}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    valueRight={false}
                    disabled={item.state === 'none'}
                    onPress={() => ifRoute(item.id)}
                    iconSelector={item.icon_selector}
                  />
                )
              case 'whatOnCard':
                return <WhatOnHorizontal key={index.toString()} data={item} />
              case 'label':
                return <LabelComponent key={index.toString()} data={item} />
              case 'gallery':
                return <GalleryComponent key={index.toString()} height={deviceWidth() / 2.38} />
              case 'memberCard':
                const {member} = profile || {}
                return (
                  <MenuItem
                    key={index.toString()}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    color={item.icon_color}
                    disabled
                    iconSelector={item.icon_selector}
                    cardStyle={styles.menuMemberShip}
                  >
                    <BarcodeBar
                      textColor={Colors().black}
                      value={member?.bepoz_account_card_number}
                      width={responsiveWidth(member?.bepoz_account_card_number?.length <= 10 ? 1.7 : 1.2)}
                      title={localize('memberNumber')}
                      text={member?.bepoz_account_number}
                      style={styles.barcodeContainer}
                    />
                  </MenuItem>
                )
              case 'voucherCard':
                return <VoucherComponent data={item} key={index.toString()} />
              case 'offerCard':
                return <OfferComponent data={item} key={index.toString()} />
              default:
                return (
                  <MenuItem
                    key={index.toString()}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    onPress={() => ifRoute(item.id)}
                    color={item.icon_color}
                    disabled={item.state === 'none'}
                    iconSelector={item.icon_selector}
                  />
                )
            }
          })}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  barcodeContainer: {
    marginTop: Metrics.marginTop
  },
  menuMemberShip: {
    backgroundColor: Colors().white
  }
})
export default HomeVersion2
