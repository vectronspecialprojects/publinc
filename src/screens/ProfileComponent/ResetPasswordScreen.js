import React, {useMemo, useState} from 'react'
import {ScrollView, StyleSheet} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import TextInputView from '../../components/TextInputView'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {NegativeButton, PositiveButton} from '../../components/ButtonView'
import {resetPassword} from '../../store/actions/authServices'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import {localize} from '../../locale/I18nConfig'
import RouteKey from '../../navigation/RouteKey'
import ScreenContainer from '../../components/ScreenContainer'

const ResetPasswordScreen = props => {
  const [oldPassword, setOldPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const dispatch = useDispatch()

  async function handleChangePassword() {
    try {
      if (validateData()) {
        dispatch(setGlobalIndicatorVisibility(true))
        await dispatch(resetPassword(oldPassword, newPassword))
        props.navigation.navigate(RouteKey.Home)
        Toast.success(localize('resetPassword.messageSuccess'))
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  function validateData() {
    if (confirmPassword !== newPassword) {
      Toast.success(localize('resetPassword.confirmNotMatch'))
      return false
    }
    return true
  }

  const disableButton = useMemo(() => {
    return !(oldPassword && newPassword && confirmPassword)
  }, [oldPassword, newPassword, confirmPassword])

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('resetPassword.title')} />
      <ScrollView style={styles.container} bounces={false}>
        <TextInputView
          floatTitle={localize('resetPassword.oldPassword')}
          secureTextEntry={true}
          value={oldPassword}
          onChangeText={text => setOldPassword(text)}
        />
        <TextInputView
          floatTitle={localize('resetPassword.newPassword')}
          secureTextEntry={true}
          value={newPassword}
          onChangeText={text => setNewPassword(text)}
        />
        <TextInputView
          floatTitle={localize('resetPassword.confirmPassword')}
          secureTextEntry={true}
          value={confirmPassword}
          onChangeText={text => setConfirmPassword(text)}
        />
        <PositiveButton
          title={localize('resetPassword.continue')}
          disabled={disableButton}
          style={{marginVertical: responsiveHeight(20)}}
          onPress={() => handleChangePassword()}
        />
        <NegativeButton
          title={localize('resetPassword.cancel')}
          onPress={() => {
            props.navigation.pop()
          }}
        />
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(20)
  },
  buttonBorder2: {
    borderWidth: 2
  }
})

export default ResetPasswordScreen
