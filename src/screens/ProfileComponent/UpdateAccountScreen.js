import React, {useCallback, useEffect, useMemo, useState} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {localize} from '../../locale/I18nConfig'
import {useDispatch, useSelector} from 'react-redux'
import Metrics, {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {updateProfile} from '../../store/actions/infoServices'
import Toast from '../../components/Toast'
import {NegativeButton, PositiveButton} from '../../components/ButtonView'
import * as api from '../../utilities/ApiManage'
import {navigate} from '../../navigation/NavigationService'
import RouteKey from '../../navigation/RouteKey'
import ProfileFieldInput from '../../components/ProfileFieldInput'
import {isEmptyValues} from '../../utilities/utils'
import ScreenContainer from '../../components/ScreenContainer'
import SubHeaderBar from '../../components/SubHeaderBar'

function UpdateAccountScreen({navigation}) {
  const profile = useSelector(state => state.infoServices.profile)
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  const [dataUpdate, setDataUpdate] = useState({})
  const [dataFields, setDataFields] = useState([])
  const [listTier, setListTier] = useState('')
  const dispatch = useDispatch()

  const validateData = useMemo(() => {
    if (isEmptyValues(dataUpdate)) {
      return false
    }
    let valid = true
    dataFields.some(item => {
      if (item.required && dataUpdate.hasOwnProperty(item.id) && !dataUpdate[item.id]) {
        valid = false
        return !valid
      }
    })
    return valid
  }, [dataFields, dataUpdate])

  const getFields = useCallback(async () => {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await api.getUserProfileSetting()
      if (!res.ok) {
        throw new Error(res?.message)
      }
      let list = res?.bepozcustomfield?.extended_value
      list = list
        .filter(item => item.displayInApp && item.shownOnProfile)
        .sort((a, b) => +a.displayOrder - +b.displayOrder)
      setDataFields(list)
      setListTier(res.data)
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    getFields()
  }, [getFields])

  const handleChangeData = useCallback(
    (key, value) => {
      setDataUpdate({
        ...dataUpdate,
        [key]: value
      })
    },
    [dataUpdate]
  )

  async function handleUpdateData() {
    try {
      let data = {...dataUpdate}
      if (data?.venue_id) {
        data.venue_id = data.venue_id?.value
      }
      await dispatch(updateProfile(data))
    } catch (e) {
      dispatch(setGlobalIndicatorVisibility(false))
      Toast.info(e.message)
    } finally {
    }
  }

  function getValue(key) {
    try {
      switch (key) {
        case 'first_name':
        case 'last_name':
        case 'dob':
          return dataUpdate?.[key] ?? profile?.member?.[key]
        case 'email':
        case 'mobile':
          return dataUpdate?.[key] ?? profile?.[key]
        case 'venue_id':
          return (
            dataUpdate?.[key] ?? {
              label: profile?.member?.current_preferred_venue_name,
              value: profile?.member?.current_preferred_venue
            }
          )
        default:
          let payload = JSON.parse(profile?.member?.payload)?.reduce((a, b) => ({...a, ...b}), {})
          return dataUpdate?.[key] ?? payload?.[key]
      }
    } catch (e) {}
  }

  function renderItem(item, index) {
    let pressable = item.id === 'email' || item.id === 'password'
    return (
      <ProfileFieldInput
        key={index.toString()}
        type={item.fieldType}
        item={item}
        isFilterVenues
        listTier={listTier}
        inputChangeHandler={handleChangeData}
        value={getValue(item.id)}
        hasConfirmPassword={false}
        editable={!!item.editOnProfile && !pressable}
        onPress={
          pressable && !!item.editOnProfile
            ? () => {
                navigate(item.id === 'password' ? RouteKey.ResetPasswordScreen : RouteKey.ChangeEmailScreen)
              }
            : null
        }
      />
    )
  }

  return (
    <ScreenContainer>
      {!useLegacyDesign && <SubHeaderBar style={styles.title} title={localize('profile.updateTitle')} />}
      <ScrollView style={styles.container}>
        {dataFields.map(renderItem)}
        <View style={styles.buttonContainer}>
          <NegativeButton
            title={'Revert changes'}
            style={styles.revertChange}
            onPress={() => {
              setDataUpdate({})
            }}
          />
          <PositiveButton
            title={'Save changes'}
            style={{flex: 1}}
            onPress={handleUpdateData}
            disabled={!validateData}
          />
        </View>
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(15),
    marginTop: Metrics.small
  },
  revertChange: {
    flex: 1,
    marginRight: responsiveWidth(15)
  },
  buttonContainer: {flexDirection: 'row', marginTop: responsiveHeight(15)}
})

export default UpdateAccountScreen
