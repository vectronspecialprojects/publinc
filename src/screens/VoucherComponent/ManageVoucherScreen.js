import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, FlatList, StyleSheet} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import SubHeaderBar from './../../components/SubHeaderBar'
import RouteKey from '../../navigation/RouteKey'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import ScreenContainer from '../../components/ScreenContainer'
import ListEmpty from '../../components/ListEmpty'
import CustomRefreshControl from '../../components/CustomRefreshControl'
import TabsBar from '../../components/TabsBar'
import {isTrue} from '../../utilities/utils'
import {getAppFlags} from '../../store/selectors/appSelector'
import {getActiveVouchers, getUnActiveVouchers} from '../../store/selectors'
import {getVouchers as apiGetVouchers} from '../../store/actions/infoServices'
import VoucherSquareItem from './components/VoucherSquareItem'
import {navigate} from '../../navigation/NavigationService'
import VoucherHorizontal from './components/VoucherHorizontal'
import Filter from '../../components/Filter'

const VOUCHER = {
  available: 101,
  all: 102
}

const ManageVoucherScreen = ({route, navigation}) => {
  const {page_name, pages} = route?.params?.params || {}
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isFilter, setIsFilter] = useState(false)
  const [selectedPage, setSelectedPage] = useState(pages[0]?.listing_type_id)
  const [selectedId, setSelectedId] = useState(0)
  const activeVouchers = useSelector(getActiveVouchers)
  const unActiveVoucher = useSelector(getUnActiveVouchers)
  const appFlags = useSelector(getAppFlags)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(apiGetVouchers())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  const isAllSelected = useMemo(() => {
    return selectedPage === VOUCHER.all
  }, [selectedPage])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const showData = useMemo(() => {
    const data = isAllSelected ? unActiveVoucher : activeVouchers
    if (selectedId === 0) {
      return data.sort((a, b) => {
        return a.id - b.id
      })
    }
    return data
      .filter(data => !data?.venue_id || data?.venue_id === selectedId)
      .sort((a, b) => {
        return a.id - b.id
      })
  }, [activeVouchers, unActiveVoucher, isAllSelected, selectedId])

  const renderItem = useCallback(
    ({item, index}) => {
      if (!isAllSelected) {
        return (
          <VoucherSquareItem
            data={item}
            onPress={() => {
              navigate(RouteKey.VoucherDetailScreen, {voucherDetail: item})
            }}
            isInProgress={!item?.barcode}
          />
        )
      }
      return (
        <VoucherHorizontal
          data={item}
          onPress={() => {
            navigate(RouteKey.VoucherDetailScreen, {voucherDetail: item, isUnavailable: true})
          }}
        />
      )
    },
    [isAllSelected]
  )

  const handleRefreshData = useCallback(() => {
    setIsRefreshing(true)
    loadContent()
  }, [loadContent])

  const filterHandler = venue => {
    setSelectedId(venue?.id)
    setIsFilter(false)
  }

  return (
    <ScreenContainer>
      <SubHeaderBar
        title={page_name}
        filterBtn={false}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      {pages?.length > 1 && (
        <TabsBar
          isShadow={isTrue(appFlags.app_is_component_shadowed)}
          pages={pages}
          onPress={setSelectedPage}
          selectedPageId={selectedPage}
        />
      )}
      <View style={styles.contentContainer}>
        <FlatList
          numColumns={isAllSelected ? 1 : 2}
          key={isAllSelected ? 'ALL' : 'ACTIVE'}
          columnWrapperStyle={isAllSelected ? null : styles.columnWrapper}
          keyExtractor={item => item.id}
          data={showData}
          renderItem={renderItem}
          ListEmptyComponent={<ListEmpty message={localize('voucher.messageNotFound')} />}
          refreshControl={<CustomRefreshControl refreshing={isRefreshing} onRefresh={handleRefreshData} />}
        />
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1
  },
  columnWrapper: {
    flex: 1,
    justifyContent: 'space-evenly'
  }
})

export default ManageVoucherScreen
