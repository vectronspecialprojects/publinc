import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import {StyleSheet, Text, View} from 'react-native'
import Metrics, {deviceWidth, FontSizes, responsiveHeight} from '../../Themes/Metrics'
import SubHeaderBar from './../../components/SubHeaderBar'
import {localize} from '../../locale/I18nConfig'
import ScreenContainer from '../../components/ScreenContainer'
import {getActiveVouchers, getUnActiveVouchers} from '../../store/selectors'
import {useSelector} from 'react-redux'
import VoucherDetailCard from './components/VoucherDetailCard'
import {Link} from '../../components/Text'
import Fonts from '../../Themes/Fonts'
import Carousel from 'react-native-reanimated-carousel/src/Carousel'
import Colors from '../../Themes/Colors'
import dayjs from 'dayjs'
import {DateTimeFormatted} from '../../constants/constants'

const ListVoucherDetailScreen = ({route, navigation}) => {
  const {voucherIndex, voucherDetail, isUnavailable} = route?.params || {}
  const unActiveVoucher = useSelector(getUnActiveVouchers)
  const activeVouchers = useSelector(getActiveVouchers)
  const listRef = useRef()
  const [index, setIndex] = useState(voucherIndex)

  const dataShow = useMemo(() => {
    return isUnavailable ? unActiveVoucher : activeVouchers
  }, [unActiveVoucher, activeVouchers, isUnavailable])

  useEffect(() => {
    const index = dataShow.findIndex(item => item.id === voucherDetail.id)
    setIndex(index)
    if (index) {
      setTimeout(() => {
        listRef.current?.next({count: index, animated: false})
      }, 100)
    }
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [])

  const renderItem = useCallback(({item, index}) => {
    return <VoucherDetailCard data={item} key={item.id.toString()} available={!isUnavailable} />
  }, [])

  const renderStatusIndicator = useCallback(() => {
    if (isUnavailable) {
      const date = voucherDetail?.redeemed ? voucherDetail?.used_date : voucherDetail?.expire_date
      return (
        <View style={styles.statusContainer(voucherDetail?.redeemed)}>
          <Text style={styles.statusText}>
            {voucherDetail?.redeemed ? 'Used' : 'Expired'} {date && '|'}{' '}
            {date && dayjs(date).format(DateTimeFormatted)}
          </Text>
        </View>
      )
    } else {
      return (
        <Link style={styles.title} centered>
          Voucher {index + 1} of {activeVouchers.length}
        </Link>
      )
    }
  }, [isUnavailable, index])

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('voucher.title')} />
      {renderStatusIndicator()}
      <Carousel
        loop={false}
        ref={listRef}
        enabled={!isUnavailable}
        width={deviceWidth()}
        style={styles.carouselContainer}
        data={dataShow}
        onSnapToItem={setIndex}
        renderItem={renderItem}
        mode={isUnavailable || dataShow?.length <= 1 ? '' : 'horizontal-stack'}
        modeConfig={{
          snapDirection: 'left',
          stackInterval: 18
        }}
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  carouselContainer: {
    flex: 1
  },
  descContainer: {
    width: '100%',
    marginTop: responsiveHeight(30)
  },
  voucherInfoContainer: {
    width: '100%',
    marginTop: responsiveHeight(30)
  },
  dot: {
    backgroundColor: 'transparent',
    borderWidth: 1
  },
  title: {
    fontSize: FontSizes.title,
    fontFamily: Fonts.bold,
    marginVertical: Metrics.marginTop
  },
  statusContainer: redeemed => ({
    height: responsiveHeight(45),
    backgroundColor: redeemed ? Colors().used : Colors().expired,
    alignItems: 'center',
    justifyContent: 'center'
  }),
  statusText: {
    fontFamily: Fonts.bold,
    fontSize: FontSizes.body,
    color: Colors().white
  }
})

export default ListVoucherDetailScreen
