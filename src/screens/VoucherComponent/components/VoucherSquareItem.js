import React from 'react'
import {ImageBackground, View, StyleSheet} from 'react-native'
import dayjs from 'dayjs'
import {CardText, HeroText} from '../../../components/Text'
import {responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import {TouchableCmp} from '../../../components/UtilityFunctions'

const VoucherSquareItem = ({data, onPress, isInProgress}) => {
  return (
    <ImageBackground style={styles.gridItem} resizeMode="contain" source={{uri: data.image_square}}>
      <TouchableCmp activeOpacity={0.6} style={{flex: 1}} onPress={onPress} disabled={isInProgress}>
        <View style={[styles.container, data.image_square_show && {backgroundColor: Colors().opacity}]}>
          {data.image_square_show && <HeroText>{data.claim_promotion?.product?.name}</HeroText>}
          {data.isInProgress && <CardText>In Progress</CardText>}
          {data.expiredDate && (
            <View style={styles.expiredContainer}>
              <HeroText>{dayjs(data.expiredDate).format('DD/MM/YYYY')}</HeroText>
            </View>
          )}
        </View>
      </TouchableCmp>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  gridItem: {
    width: responsiveWidth(150),
    aspectRatio: 1,
    marginTop: responsiveHeight(20),
    borderRadius: responsiveWidth(5),
    overflow: 'hidden'
  },
  container: {
    flex: 1,
    borderRadius: responsiveWidth(5),
    paddingHorizontal: responsiveWidth(10),
    paddingTop: '30%',
    alignItems: 'center'
  },
  expiredContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
    paddingBottom: responsiveHeight(5)
  }
})

export default VoucherSquareItem
