import React, {useCallback} from 'react'
import CardContainer from '../../../components/CardContainer'
import Metrics, {
  deviceWidth,
  FontSizes,
  responsiveHeight,
  responsiveWidth,
  shadow
} from '../../../Themes/Metrics'
import {StyleSheet, View} from 'react-native'
import FastImage from 'react-native-fast-image'
import Colors, {getColorOpacity} from '../../../Themes/Colors'
import {CardText} from '../../../components/Text'
import BarcodeBar from '../../../components/BarcodeBar'
import Fonts from '../../../Themes/Fonts'

import dayjs from 'dayjs'
import {DateTimeFormatted} from '../../../constants/constants'

const VoucherDetailCard = ({data, available}) => {
  const renderCoverBackground = useCallback(() => {
    if (available) {
      return null
    }
    return <View style={styles.coverBackground(data.redeemed)} />
  }, [data, available])

  return (
    <CardContainer style={styles.container} isShadow={true}>
      <FastImage source={{uri: data.image_card}} style={styles.headerImage} />
      <View style={styles.textContainer}>
        <CardText style={styles.title}>{data?.claim_promotion?.product?.name}</CardText>
        <CardText>{data?.claim_promotion?.product?.desc}</CardText>
      </View>
      <View style={styles.barcodeContainer}>
        {!!data.expire_date && (
          <CardText style={styles.expireDate}>
            Expiry: {dayjs(data.expire_date).format(DateTimeFormatted)}
          </CardText>
        )}
        {!!data?.barcode && (
          <BarcodeBar
            value={data?.barcode}
            width={responsiveWidth(1.5)}
            title={''}
            text={data?.barcode}
            textColor={Colors().black}
            titleStyle={styles.barcodeTitle}
          />
        )}
      </View>
      {renderCoverBackground()}
    </CardContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '92%',
    marginTop: Metrics.marginTop,
    borderRadius: Metrics.borderRadiusHuge,
    marginHorizontal: Metrics.marginHorizontal,
    ...shadow
  },
  headerImage: {
    width: deviceWidth() - Metrics.marginHorizontal * 2,
    height: responsiveHeight(116),
    borderTopLeftRadius: Metrics.borderRadiusHuge,
    borderTopRightRadius: Metrics.borderRadiusHuge
  },
  title: {
    fontSize: FontSizes.title,
    fontFamily: Fonts.bold,
    marginVertical: Metrics.marginVertical
  },
  textContainer: {
    paddingHorizontal: Metrics.marginHorizontal,
    flex: 1
  },
  barcodeContainer: {
    backgroundColor: Colors().white,
    borderTopWidth: 0.5,
    borderColor: Colors().gray,
    flex: 1,
    borderBottomLeftRadius: Metrics.borderRadiusHuge,
    borderBottomRightRadius: Metrics.borderRadiusHuge,
    justifyContent: 'space-evenly'
  },
  expireDate: {
    textAlign: 'center'
  },
  barcodeTitle: {
    marginTop: Metrics.marginTop
  },
  coverBackground: redeemed => ({
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: getColorOpacity(redeemed ? Colors().used : Colors().expired, 0.2),
    borderRadius: Metrics.borderRadiusHuge
  })
})

export default React.memo(VoucherDetailCard)
