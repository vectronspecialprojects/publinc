import React from 'react'
import {TouchableOpacity, StyleSheet, View} from 'react-native'
import {BackgroundText, Text} from '../../../components/Text'
import Metrics from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Fonts from '../../../Themes/Fonts'

export const VoucherHorizontal = ({data, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <BackgroundText style={styles.item} numberOfLines={1}>
        {data.claim_promotion?.product?.name}
      </BackgroundText>
      <View style={styles.status(data.redeemed)}>
        <Text style={styles.statusText}>{data.redeemed ? 'Used' : 'Expired'}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: Metrics.marginTop,
    marginHorizontal: Metrics.marginHorizontal
  },
  item: {
    flex: 3
  },
  statusText: {
    fontFamily: Fonts.bold,
    color: Colors().white
  },
  status: redeemed => ({
    flex: 1,
    height: Metrics.large,
    borderRadius: Metrics.large / 2,
    backgroundColor: redeemed ? Colors().used : Colors().expired,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Metrics.marginHorizontal
  })
})

export default VoucherHorizontal
