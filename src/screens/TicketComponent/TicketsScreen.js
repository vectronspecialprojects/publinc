import React, {useEffect, useState, useCallback, useMemo} from 'react'
import {FlatList} from 'react-native'
import * as infoServicesActions from '../../store/actions/infoServices'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import TicketItem from './components/TicketItem'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import RouteKey from '../../navigation/RouteKey'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import ButtonVenue from '../../components/ButtonVenue'
import ScreenContainer from '../../components/ScreenContainer'
import ListEmpty from '../../components/ListEmpty'
import CustomRefreshControl from '../../components/CustomRefreshControl'

const TicketsScreen = ({route, navigation}) => {
  const tickets = useSelector(state => state.infoServices.tickets)
  const selectedId = useSelector(state => state.infoServices.preferredVenueId)
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchTickets())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const showData = useMemo(() => {
    if (tickets === undefined) {
      return []
    }
    if (tickets !== undefined) {
      if (selectedId === 0) {
        return tickets
      }
      return tickets.filter(
        ticket => ticket.order_detail?.venue?.id === 0 || ticket.order_detail?.venue?.id === selectedId
      )
    }
  }, [tickets, selectedId])

  function renderItem({item, index}) {
    return (
      <TicketItem
        data={item}
        onPress={() =>
          navigation.navigate(RouteKey.TicketDetailScreen, {
            ticketDetail: item
          })
        }
      />
    )
  }

  return (
    <ScreenContainer>
      <ButtonVenue />
      <FlatList
        data={showData}
        style={{marginHorizontal: responsiveWidth(25), marginBottom: responsiveHeight(10)}}
        renderItem={renderItem}
        numColumns={2}
        ListEmptyComponent={<ListEmpty message={'No Ticket found, please check again later.'} />}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      />
    </ScreenContainer>
  )
}

export default TicketsScreen
