import React from 'react'
import {Text} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import ScreenContainer from '../../components/ScreenContainer'

const ShopsScreen = props => {
  const title = props.route.params.params?.page_name
  return (
    <ScreenContainer>
      <SubHeaderBar title={title} filterBtn={false} />
      <Text>This is ShopsScreen</Text>
    </ScreenContainer>
  )
}

export default ShopsScreen
