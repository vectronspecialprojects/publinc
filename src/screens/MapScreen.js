import React from 'react'
import {StyleSheet} from 'react-native'
import MapView from 'react-native-maps'
import FastImage from 'react-native-fast-image'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'

const MapScreen = props => {
  const mapRegion = {
    latitude: props.route.params.location.lat,
    longitude: props.route.params.location.lng,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421
  }
  const marker = [
    {
      latitude: props.route.params.location.lat,
      longitude: props.route.params.location.lng,
      title: props.route.params.location.name,
      subtitle: props.route.params.location.address
    }
  ]
  return (
    <MapView style={styles.map} region={mapRegion} annotations={marker} provider={MapView.PROVIDER_GOOGLE}>
      <MapView.Marker
        coordinate={{
          latitude: marker[0].latitude,
          longitude: marker[0].longitude
        }}
        title={marker[0].title}
        description={marker[0].subtitle}
      >
        <FastImage
          source={require('../assets/img/marker.png')}
          style={styles.marker}
          resizeMode={'contain'}
        />
      </MapView.Marker>
    </MapView>
  )
}

const styles = StyleSheet.create({
  map: {
    flex: 1
  },
  marker: {
    width: responsiveWidth(40),
    height: responsiveHeight(50)
  }
})

export default MapScreen
