import React, {useCallback, useEffect, useImperativeHandle, useState} from 'react'
import {View, StyleSheet, Modal, TouchableOpacity, Text, Switch, ScrollView, FlatList} from 'react-native'
import Colors from '../../../Themes/Colors'
import Metrics, {
  deviceWidth,
  responsiveFont,
  responsiveHeight,
  responsiveWidth
} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {apiGetListCard} from '../../../utilities/ApiManage'
import TextInputView from '../../../components/TextInputView'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ButtonView, {NegativeButton, PositiveButton} from '../../../components/ButtonView'
import BottomSheet from '../../../components/BottomSheet'
import {CardText} from '../../../components/Text'
import ComponentContainer from '../../../components/ComponentContainer'
import {CardField} from '@stripe/stripe-react-native'

function PaymentPopup({visible, onClose, amount, onSubmit}, ref) {
  const [listCard, setListCard] = useState('')
  const [number, setNumber] = useState('')
  const [isSave, setSave] = useState(false)
  const [isUseSaveCard, setUseSaveCard] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [isAddNewCard, setAddNewCard] = useState(false)
  const [token, setToken] = useState('')
  const [brand, setBrand] = useState('')
  const [showSelectCard, setShowSelectCard] = useState(false)

  const getListCard = useCallback(async () => {
    try {
      const res = await apiGetListCard()
      if (!res.ok) {
        throw new Error(res.message)
      }
      setListCard(res.data?.cards?.data)
      if (res.data?.cards?.data?.length > 0) {
        handleSelectCard(res.data?.cards?.data[0])
      }
    } catch (e) {}
  }, [])

  useEffect(() => {
    getListCard()
  }, [getListCard])

  useImperativeHandle(ref, () => ({
    setError: message => {
      setErrorMessage(message)
    }
  }))

  function handleSelectCard(card) {
    const {id, brand, last4} = card || {}
    setToken(id)
    setNumber(last4)
    setUseSaveCard(true)
    setBrand(brand)
  }

  return (
    <Modal
      visible={visible}
      transparent={true}
      onRequestClose={() => {}}
      animationType={'fade'}
      animated={true}
    >
      <View style={styles.popContainer}>
        <ComponentContainer style={styles.container}>
          <ScrollView>
            <View style={{padding: responsiveWidth(15)}}>
              <CardText style={styles.title}>Credit card details</CardText>
              <CardText style={[styles.title, {fontSize: responsiveFont(15)}]}>
                Please enter the card details
              </CardText>
              {!!listCard && !isAddNewCard && (
                <TextInputView
                  value={`${brand} xxxx xxxx xxxx ${number}`}
                  rightIcon={
                    <MaterialIcons
                      name={'keyboard-arrow-down'}
                      size={25}
                      color={Colors().formInputFieldsText}
                    />
                  }
                  onPress={() => setShowSelectCard(true)}
                  editable={false}
                />
              )}
              {isAddNewCard && (
                <CardField
                  postalCodeEnabled={false}
                  cardStyle={{textColor: '#000'}}
                  style={styles.cardContainer}
                />
              )}

              <View style={{flexDirection: 'row'}}>
                <CardText style={styles.textSaveCard}>Would you like to save this card?</CardText>
                <Switch
                  value={isSave}
                  onValueChange={value => setSave(value)}
                  tintColor={Colors().heroText}
                />
              </View>
              {!!errorMessage && (
                <Text style={{color: Colors().errorText, marginTop: responsiveHeight(10)}}>
                  {errorMessage}
                </Text>
              )}
              {!!listCard && (
                <ButtonView
                  title={isAddNewCard ? 'Selected a Saved Card' : 'Add New Card'}
                  style={{
                    marginTop: responsiveHeight(30)
                  }}
                  onPress={() => {
                    setAddNewCard(!isAddNewCard)
                    if (isAddNewCard) {
                      handleSelectCard(listCard[0])
                      setUseSaveCard(true)
                    } else {
                      setUseSaveCard(false)
                    }
                  }}
                />
              )}
              <CardText style={styles.total}>Total Amount: ${amount.toFixed(2)}</CardText>
            </View>
          </ScrollView>
          <View style={{flexDirection: 'row'}}>
            <NegativeButton title={'Cancel'} style={styles.cancelButton} onPress={onClose} />
            <PositiveButton
              title={'Continue'}
              style={styles.button}
              onPress={() =>
                onSubmit({
                  save_card: isSave,
                  token,
                  use_saved_card: isUseSaveCard
                })
              }
            />
          </View>
        </ComponentContainer>
      </View>
      <BottomSheet
        visible={showSelectCard}
        headerTitle={'Select A Card'}
        showConfirmButton={false}
        onClose={() => setShowSelectCard(false)}
      >
        <FlatList
          data={listCard}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                style={[styles.itemCard, {borderColor: Colors().gray}]}
                onPress={() => {
                  handleSelectCard(item)
                  setShowSelectCard(false)
                }}
              >
                <Text
                  style={{
                    fontFamily: Fonts.bold,
                    fontSize: responsiveFont(15)
                  }}
                >{`${item.brand} xxxx xxxx xxxx ${item.last4}`}</Text>
                <Text>Exp: {`${item.exp_month}/${item.exp_year}`}</Text>
              </TouchableOpacity>
            )
          }}
        />
      </BottomSheet>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    width: deviceWidth() * 0.9,
    borderRadius: responsiveWidth(14),
    overflow: 'hidden'
  },
  button: {
    flex: 1,
    borderRadius: 0
  },
  total: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.bold,
    textAlign: 'center',
    marginVertical: responsiveHeight(30)
  },
  title: {
    fontSize: responsiveFont(16),
    fontFamily: Fonts.bold,
    textAlign: 'center',
    marginBottom: responsiveHeight(18)
  },
  textSaveCard: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.regular,
    flex: 1
  },
  itemCard: {
    height: responsiveHeight(50),
    justifyContent: 'center',
    borderBottomWidth: 0.5
  },
  cancelButton: {
    borderRadius: 0,
    borderBottomLeftRadius: responsiveWidth(14),
    flex: 1,
    marginRight: 3
  },
  cardContainer: {
    height: 50,
    marginBottom: Metrics.marginTop
  },
  popContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: Colors().opacity
  }
})

export default React.forwardRef(PaymentPopup)
