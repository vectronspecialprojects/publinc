import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveHeight} from '../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import Colors from '../Themes/Colors'
import CardItem from './../components/CardItem'
import {Card} from '../modals/modals'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {isTrue} from '../utilities/utils'
import ButtonVenue from '../components/ButtonVenue'
import ScreenContainer from '../components/ScreenContainer'
import ListEmpty from '../components/ListEmpty'
import CustomRefreshControl from '../components/CustomRefreshControl'

const StampcardScreen = () => {
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const preferredVenueId = useSelector(state => state.infoServices.preferredVenueId)
  const appFlags = useSelector(state => state.app.appFlags)
  const [selectedId, setSelectedId] = useState(preferredVenueId)
  const preferredBepozVenueId = useSelector(state => state.infoServices.preferredBepozVenueId)
  const [selectedBepozVenue, setSelectedBepozVenue] = useState(preferredBepozVenueId)
  const stampCards = useSelector(state => state.infoServices.stampCards)

  useEffect(() => {
    setSelectedId(preferredVenueId)
    setSelectedBepozVenue(preferredBepozVenueId)
  }, [preferredVenueId, preferredBepozVenueId])

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(7))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const showData = useMemo(() => {
    if (stampCards !== undefined) {
      // for stampcard
      //  for single venue
      if (!isTrue(appFlags?.isMultipleVenue)) {
        return stampCards?.sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
      }

      //  for multi venues and bepoz venue id not setup in venue
      if (!selectedBepozVenue) {
        return []
      }
      //  for multi venues and venue setup with bepoz venue id
      return stampCards
        ?.filter(data => data?.listing?.venue?.id === 0 || data?.listing?.venue?.id === selectedId)
        .sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
    }
  }, [stampCards, appFlags?.isMultipleVenue, selectedBepozVenue, selectedId])

  const renderItem = itemData => {
    let accumulation = 0
    if (
      !!itemData?.item?.listing?.prize_promotion?.promo_account &&
      typeof itemData?.item?.listing?.prize_promotion?.promo_account === 'object'
    ) {
      if (isTrue(appFlags?.isMultipleVenue)) {
        accumulation =
          itemData?.item?.listing?.prize_promotion?.promo_account?.filter(
            list => +list?.venue_id === selectedBepozVenue
          )[0]?.accumulation || 0
      } else {
        itemData?.item?.listing?.prize_promotion?.promo_account.forEach(
          list => (accumulation += +list.accumulation)
        )
      }
    }

    const listing = itemData?.item?.listing
    const cardDetail = new Card(
      listing?.heading,
      listing?.desc_short,
      0,
      +listing?.prize_promotion?.needed - +accumulation,
      +accumulation,
      listing?.image_banner
    )

    return (
      <CardItem
        style={{marginBottom: responsiveHeight(5)}}
        titleStyle={{color: Colors().heroText}}
        touchDisabled={true}
        cardDetail={cardDetail}
      />
    )
  }

  return (
    <ScreenContainer>
      <ButtonVenue />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={item => item?.id.toString()}
        ListEmptyComponent={<ListEmpty message={localize('stampCard.stampCartNotFound')} />}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      />
    </ScreenContainer>
  )
}

export default StampcardScreen
