import React from 'react'
import {StyleSheet} from 'react-native'
import Colors from '../../Themes/Colors'
import {TouchableCmp} from '../../components/UtilityFunctions'
import {FontSizes, responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import {CardText} from '../../components/Text'
import ComponentContainer from '../../components/ComponentContainer'

const ItemList = props => {
  const {isSelected} = props
  return (
    <TouchableCmp onPress={props.onPress}>
      <ComponentContainer style={[styles.container, props.style]}>
        <CardText style={props.textStyle} fontSize={FontSizes.body}>
          {props.data.name}
        </CardText>
        {props.isArrowShowed && (
          <FontAwesome5Pro
            name={isSelected ? 'chevron-down' : 'chevron-right'}
            size={responsiveFont(10)}
            color={Colors().cardText}
          />
        )}
      </ComponentContainer>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: responsiveWidth(20),
    marginTop: responsiveHeight(25)
  }
})

export default ItemList
