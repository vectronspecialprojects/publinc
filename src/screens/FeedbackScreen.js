import React, {useEffect, useState, useCallback} from 'react'
import {View, StyleSheet, TextInput, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {AirbnbRating} from 'react-native-ratings'
import {responsiveHeight, responsiveWidth, responsiveFont} from '../Themes/Metrics'
import SubHeaderBar from './../components/SubHeaderBar'
import {PositiveButton} from './../components/ButtonView'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import analytics from '@react-native-firebase/analytics'
import Toast from '../components/Toast'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import CircleCheckBox from '../components/CircleCheckBox'
import ScreenContainer from '../components/ScreenContainer'
import {CardText} from '../components/Text'

const FeedbackScreen = props => {
  const title = props.route.params.params.page_name
  const dispatch = useDispatch()
  const [rating, setRating] = useState(0)
  const [comment, setComment] = useState('')
  const [isChecked, setIsChecked] = useState(false)

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'Feedbacks'})
    }

    firebase()
  }, [])

  const submitHandler = useCallback(async () => {
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      const rest = await dispatch(infoServicesActions.submitFeedback(rating, comment, isChecked))
      Toast.success(rest)
      props.navigation.goBack()
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    dispatch(setGlobalIndicatorVisibility(false))
    setRating(0)
    setComment('')
    setIsChecked(false)
  }, [dispatch, rating, comment, isChecked, props.navigation])

  return (
    <ScreenContainer>
      <SubHeaderBar title={title} />
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={{paddingHorizontal: responsiveWidth(20)}}>
          <View
            style={{
              ...Styles.centerContainer,
              ...{height: responsiveHeight(56), marginTop: responsiveHeight(20)}
            }}
          >
            <CardText style={Styles.xSmallNormalText}>{localize('feedback.description')}</CardText>
          </View>

          <AirbnbRating
            count={5}
            defaultRating={0}
            size={responsiveFont(28)}
            showRating={false}
            selectedColor={Colors().linkText}
            onFinishRating={rating => setRating(rating)}
          />

          <TextInput
            style={styles.comment(
              Colors().formInputFieldsFill,
              Colors().formInputFieldsBorder,
              Colors().formInputFieldsText
            )}
            value={comment}
            onChangeText={text => setComment(text)}
            multiline={true}
            underlineColorAndroid="transparent"
            maxLength={200}
          />

          <View style={{...Styles.centerContainer, ...{marginTop: responsiveHeight(30)}}}>
            <CircleCheckBox
              outerColor={Colors().heroFill}
              innerColor={Colors().heroFill}
              value={isChecked}
              onPress={() => setIsChecked(!isChecked)}
              title={localize('feedback.plsContact')}
            />
          </View>

          <View style={{marginTop: responsiveHeight(100)}}>
            <PositiveButton
              title={localize('feedback.submit')}
              disabled={!rating && !isChecked && !comment.length}
              onPress={submitHandler}
            />
          </View>
        </View>
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  comment: (backgroundColor, borderColor, color) => ({
    marginTop: responsiveHeight(40),
    paddingHorizontal: responsiveWidth(20),
    paddingVertical: responsiveWidth(5),
    height: responsiveHeight(164),
    width: '100%',
    borderRadius: 15,
    alignSelf: 'center',
    textAlignVertical: 'top',
    lineHeight: responsiveHeight(23),
    borderWidth: StyleSheet.hairlineWidth,
    backgroundColor,
    color,
    borderColor
  })
})

export default FeedbackScreen
