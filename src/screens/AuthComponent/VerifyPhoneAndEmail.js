import React, {useCallback, useEffect, useReducer, useState} from 'react'
import {View, StyleSheet, ScrollView, KeyboardAvoidingView} from 'react-native'
import {localize} from '../../locale/I18nConfig'
import {PositiveButton} from '../../components/ButtonView'
import SubHeaderBar from '../../components/SubHeaderBar'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {apiVerifyPhoneAndEmail, getMatchInstruction} from '../../utilities/ApiManage'
import RouteKey from '../../navigation/RouteKey'
import Toast from '../../components/Toast'
import {isIOS, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Html from '../../components/Html'
import {onLinkPress} from '../../components/UtilityFunctions'
import TextInputView from '../../components/TextInputView'
import {getStatusBarHeight} from '../../utilities/utils'
import ScreenContainer from '../../components/ScreenContainer'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    }
  }
  return state
}

function VerifyPhoneAndEmail({navigation}) {
  const [instruction, setInstruction] = useState('')
  const dispatch = useDispatch()

  useEffect(() => {
    getMatchInstruction().then(res => {
      if (res.ok) {
        setInstruction(res?.match_email_mobile)
      }
    })
  }, [])

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      mobile: '',
      email: ''
    },
    inputValidities: {
      mobile: false,
      email: false
    },
    formIsValid: false
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      })
    },
    [dispatchFormState]
  )

  async function handleMatchAccount() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await apiVerifyPhoneAndEmail(formState.inputValues.mobile, formState.inputValues.email)
      if (res.ok) {
        navigation.navigate(RouteKey.MatchAccountInfo, {
          data: res.data?.AccountFull
        })
      } else {
        if (res.code === 'bepoz_account_exist') {
          throw new Error(res?.message)
        }
        navigation.navigate(RouteKey.MatchAccountScreen)
      }
    } catch (e) {
      Toast.info(e?.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('verifyPhoneAndEmail.title')} alignLeft />
      <KeyboardAvoidingView
        behavior={isIOS() ? 'padding' : 'height'}
        keyboardVerticalOffset={getStatusBarHeight()}
        style={styles.container}
      >
        <ScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false}>
          <Html html={instruction} onLinkPress={onLinkPress} />
          <TextInputView
            id="email"
            floatTitle={localize('verifyPhoneAndEmail.email')}
            placeholder={localize('verifyPhoneAndEmail.email')}
            keyboardType="email-address"
            required
            email
            // emailAvailability
            autoCapitalize="none"
            messageError={`${localize('matchAccount.plsEnter')} ${localize('verifyPhoneAndEmail.email')}`}
            onChangeText={inputChangeHandler}
            value={formState.inputValues.email}
            initialValue=""
          />
          <TextInputView
            id="mobile"
            floatTitle={localize('verifyPhoneAndEmail.mobile')}
            placeholder={localize('verifyPhoneAndEmail.mobile')}
            keyboardType="phone-pad"
            required
            minLength={10}
            maxLength={30}
            autoCapitalize="none"
            messageError={`${localize('matchAccount.plsEnter')} ${localize('verifyPhoneAndEmail.mobile')}`}
            onChangeText={inputChangeHandler}
            initialValue=""
            value={formState.inputValues.mobile}
          />
          <View style={{flex: 1}} />
          <PositiveButton
            title={localize('verifyPhoneAndEmail.connect')}
            disabled={!formState.formIsValid}
            style={styles.buttonBorder}
            onPress={() => handleMatchAccount()}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: responsiveWidth(24)
  },
  buttonBorder: {
    borderWidth: 2,
    marginVertical: responsiveHeight(10),
    marginBottom: responsiveHeight(40)
  }
})

export default VerifyPhoneAndEmail
