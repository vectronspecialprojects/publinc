import React, {useReducer, useState, useCallback, useEffect} from 'react'
import {View, StyleSheet, ScrollView, KeyboardAvoidingView} from 'react-native'
import CheckBox from '../../components/CheckBox'
import Fonts from '../../Themes/Fonts'
import SubHeaderBar from '../../components/SubHeaderBar'
import {NegativeButton, PositiveButton} from '../../components/ButtonView'
import TermAndCondition from '../../components/TermAndCondition'
import {isIOS, responsiveHeight, responsiveWidth, responsiveFont} from '../../Themes/Metrics'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import {signupMatch} from '../../store/actions/authServices'
import {
  getKeyboardVerticalOffset,
  getStateFromPostCode,
  isTrue,
  parseStringToJson
} from '../../utilities/utils'
import * as api from '../../utilities/ApiManage'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import ProfileFieldInput from '../../components/ProfileFieldInput'
import dayjs from 'dayjs'
import ScreenContainer from '../../components/ScreenContainer'
import {Link} from '../../components/Text'

const BY_VENUE = 'by_venue'
const BY_VENUE_TAG = 'by_venue_tag'
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    }
  }
  return state
}

const MATCH_DATA = {
  first_name: 'FirstName',
  last_name: 'LastName',
  email: 'Email1st',
  mobile: 'Mobile',
  post_code: 'PCode',
  title: 'Title',
  street: 'Street',
  city: 'City',
  state: 'State',
  gender: 'Gender',
  tier_id: 'Tier',
  dob: 'DateBirth'
}

const DISABLE = []

function MatchAccountInfo(props) {
  const dispatch = useDispatch()
  const {data} = props.route?.params || {}
  const [listTier, setListTier] = useState('')
  const [options, setOptions] = useState([])
  const memberDefaultTierDetail = useSelector(state => state.app.memberDefaultTierDetail)
  const memberDefaultTierOption = useSelector(state => state.app.memberDefaultTierOption)
  const [agreeTermAndCondition, setAgreeTermCondition] = useState(false)
  const [optOutMarketing, setOptOutMarketing] = useState(false)
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      password: '',
      password_confirmation: ''
    },
    inputValidities: {
      password: false,
      password_confirmation: false
    },
    formIsValid: false
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        input: inputIdentifier,
        value: inputValue,
        isValid: inputValidity
      })
    },
    [dispatchFormState]
  )

  const getProfileSetting = useCallback(async () => {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const response = await api.getUserProfileSetting()
      let hasTier = false
      if (!response.ok) {
        throw new Error(response?.message)
      }
      let list = parseStringToJson(response?.bepozcustomfield?.extended_value)
      list.filter(item => isTrue(item.displayInApp)).sort((a, b) => +a.displayOrder - +b.displayOrder)
      list?.map(e => {
        let payload = {
          type: FORM_INPUT_UPDATE,
          input: e?.id,
          value: data?.[MATCH_DATA[e?.id]] || '',
          isValid: !!data?.[MATCH_DATA[e?.id]] || !e.required
        }
        if (e?.id === 'dob') {
          payload.value = payload.value ? dayjs(payload.value).format('ddd, MMM D, YYYY') : ''
        }
        if (e?.fieldType === 'Text' && e?.id !== 'email') {
          payload.value = payload?.value?.replace(/[^a-zA-Z0-9#]/g, '')
        }
        dispatchFormState(payload)
        if (e.fieldType === 'Tier') {
          hasTier = true
        }
      })
      if (hasTier) {
        setListTier(response.data)
      }
      setOptions(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetTier'), e.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [data, dispatch])

  useEffect(() => {
    getProfileSetting()
  }, [getProfileSetting])

  useEffect(() => {
    if (memberDefaultTierOption === BY_VENUE && formState.inputValues?.venue?.value) {
      let tierId = +memberDefaultTierDetail?.[formState.inputValues?.venue?.value]?.tier
      if (tierId) {
        inputChangeHandler('tier', tierId, true)
      }
    }
  }, [formState.inputValues?.venue, inputChangeHandler, memberDefaultTierDetail, memberDefaultTierOption])

  useEffect(() => {
    if (formState.inputValues?.post_code?.length >= 3) {
      const state = getStateFromPostCode(+formState.inputValues?.post_code)
      inputChangeHandler('state', state, true)
      if (memberDefaultTierOption === BY_VENUE_TAG && state) {
        const tierId = +memberDefaultTierDetail?.[state]?.tier
        if (tierId) {
          inputChangeHandler('tier', tierId, true)
        }
      }
    }
  }, [formState.inputValues?.post_code, inputChangeHandler, memberDefaultTierDetail, memberDefaultTierOption])

  async function handleSubmitData() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      await dispatch(
        signupMatch({
          AccountID: data?.AccountID,
          AccNumber: data?.AccNumber,
          CardNumber: data?.CardNumber,
          UseCaLinkBalance: isTrue(data?.UseCaLinkBalance),
          UseCaLinkPoints: isTrue(data?.UseCaLinkPoints),
          share_info: true,
          opt_out_marketing: optOutMarketing,
          ...formState.inputValues,
          venue_id: formState.inputValues.venue_id?.value
        })
      )
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const renderSignUpFields = useCallback(() => {
    return options?.map((item, index) => {
      return (
        <ProfileFieldInput
          key={index.toString()}
          type={item.fieldType}
          item={item}
          listTier={listTier}
          formState={formState}
          inputChangeHandler={inputChangeHandler}
          editable={!DISABLE.includes(item.id)}
          checkInitialEmail={true}
        />
      )
    })
  }, [options, listTier, formState, inputChangeHandler])

  return (
    <ScreenContainer>
      <SubHeaderBar title="Match My Account" />
      <KeyboardAvoidingView
        style={{paddingHorizontal: responsiveWidth(20), flex: 1}}
        behavior={isIOS() ? 'padding' : null}
        keyboardVerticalOffset={getKeyboardVerticalOffset()}
      >
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{paddingTop: responsiveHeight(10)}}
        >
          {renderSignUpFields()}
          <View style={{marginTop: responsiveHeight(12)}}>
            <Link style={styles.checkBoxTitle}>No thanks, I don’t want exclusive offers</Link>
            <CheckBox
              size={responsiveHeight(35)}
              title={localize('signUp.receiveOffer')}
              value={optOutMarketing}
              onPress={() => setOptOutMarketing(!optOutMarketing)}
            />
          </View>
          <Link style={styles.checkBoxTitle}>Terms & Conditions</Link>
          <TermAndCondition
            onCheck={() => setAgreeTermCondition(!agreeTermAndCondition)}
            value={agreeTermAndCondition}
            allowCheck={true}
          />
          <View style={styles.buttonContainer}>
            <PositiveButton
              disabled={!formState.formIsValid || !agreeTermAndCondition}
              title={'Continue'}
              onPress={() => handleSubmitData()}
              style={{marginTop: responsiveHeight(10)}}
            />
            <NegativeButton
              title={'Cancel'}
              style={styles.buttonBorder}
              onPress={() => {
                props.navigation.pop()
              }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  buttonBorder: {
    borderWidth: 2,
    marginVertical: responsiveHeight(10)
  },
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(70),
    flex: 1
  },
  checkBoxTitle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    marginBottom: responsiveHeight(5)
  }
})

export default MatchAccountInfo
