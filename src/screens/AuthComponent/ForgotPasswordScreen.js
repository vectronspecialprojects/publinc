import React, {useReducer, useCallback} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import * as infoServicesActions from '../../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import SubHeaderBar from '../../components/SubHeaderBar'
import {PositiveButton} from '../../components/ButtonView'
import Toast from '../../components/Toast'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import TextInputView from '../../components/TextInputView'
import ScreenContainer from '../../components/ScreenContainer'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    }
  }
  return state
}

const ForgotPasswordScreen = () => {
  const dispatch = useDispatch()
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      email: ''
    },
    inputValidities: {
      email: false
    },
    formIsValid: false
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      })
    },
    [dispatchFormState]
  )

  const submitHandler = async () => {
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      await dispatch(infoServicesActions.resetPassword(formState.inputValues.email))
      Toast.success(localize('forgotPassword.success'))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('forgotPassword.title')} />
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={{flex: 1, paddingHorizontal: responsiveWidth(20), marginTop: responsiveHeight(20)}}>
          <TextInputView
            id="email"
            title={localize('forgotPassword.email')}
            label={localize('forgotPassword.email')}
            keyboardType="email-address"
            required
            email
            autoCapitalize="none"
            messageError={localize('forgotPassword.plsEnter')}
            onChangeText={inputChangeHandler}
            initialValue=""
            value={formState.inputValues?.value}
          />

          <PositiveButton
            title={'Submit'}
            disabled={!formState.formIsValid}
            onPress={submitHandler}
            style={styles.button}
          />
        </View>
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  button: {
    marginTop: responsiveHeight(100)
  }
})

export default ForgotPasswordScreen
